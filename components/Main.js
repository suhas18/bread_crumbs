import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Breadcrumb from './Breadcrumb';
import BreadcrumbItems from './BreadcrumbItems';
import Screen1 from './screens/Screen1';

const Main = () => {
    return (
        <View>
            <Breadcrumb seperator='/'   >
            <BreadcrumbItems href="Home" item="Home" />
            <BreadcrumbItems href="Screen 1" item="Screen 1" component={Screen1} />
            <BreadcrumbItems href="Screen 2" item="Screen 2"  />
            <BreadcrumbItems href="Screen 3" item="Screen 3" />
            <BreadcrumbItems href="Screen 4" item="Screen 4" />
            </Breadcrumb>
        </View>
    );
}

const styles = StyleSheet.create({})

export default Main;


