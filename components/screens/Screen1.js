import React from 'react';
import {View, StyleSheet, Text} from 'react-native';

const Screen1 = () => {
    console.log("Pressed Screen 1");
    return (
        <View >
            <Text>This is Screen 1</Text>
        </View>
    );
}

const styles = StyleSheet.create({})

export default Screen1;
