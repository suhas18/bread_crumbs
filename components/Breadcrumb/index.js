import React from 'react';
import {View, StyleSheet, Text, SafeAreaView, FlatList, TouchableOpacity, Pressable, Button} from 'react-native';
import { useState } from 'react';

const Breadcrumb= ({children,seperator,maxItems}) => {
const [expand,setExpand] = useState(true)
    console.log(children, '####');

  const Pressed = () =>{
    console.log("Pressed");
    setExpand(!expand)
    console.log(expand);
  }

    return (
        <SafeAreaView>
            <FlatList  horizontal showsHorizontalScrollIndicator={false}
            data={children}
            contentContainerStyle={{flexDirection:'row',marginLeft:10}}
            renderItem={({ item, index })=>{
                    const isEnd = index === children.length - 1;
     
                   if(expand) {
                    return (
                        <View style={{flexDirection:'row'}}  >
                        {item}
                       
                       {/* {  console.log(item) } */}
                       { !isEnd ? (
                          <TouchableOpacity onPress={()=>setExpand(!expand)}>
                            <Text style={{marginRight:10,marginLeft:10}}>{seperator}</Text>
                          </TouchableOpacity> 
                       )
                       : null
                       }
                   </View>
                    )
                   } else if(index === 1 ) {
                    return (
                        <View style={{flexDirection:'row'}}>{item}
                                <Pressable onPress={()=>Pressed()}><Text>  ...  </Text></Pressable>
                        </View>
                    )
                   } else if (index === children?.length - 1) {
                    return (
                        <View style={{flexDirection:'row'}}>{item}
                        </View>
                    )
                   }
                     
            }}>

            </FlatList>

        </SafeAreaView>
    );
}

const styles = StyleSheet.create({

})

export default Breadcrumb;
